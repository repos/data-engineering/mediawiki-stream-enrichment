# mediawiki-stream-enrichment

Implementation of [T307959](https://phabricator.wikimedia.org/T307959).

A POC Flink Service to Combine Existing Streams, Enrich and Output to New Topic.

# Test and build

Test with
```bash
./mvnw clean verify
```

Build a jar with dependencies with
```bash
./mvnw clean package
```

# Deploy on YARN

# Deploy

A standalone cluster can be setup locally (on a stat machine atop YARN) with
```
wget https://dlcdn.apache.org/flink/flink-1.15.0/flink-1.15.0-bin-scala_2.12.tgz
tar xvzf flink-1.15.0-bin-scala_2.12.tgz
cd flink-1.15.0
export HADOOP_CLASSPATH=`hadoop classpath`
./bin/yarn-session.sh --detached
```

The `package` target can be manually copied to a stat machine with:
```bash
scp  target/enrichment-1.0-SNAPSHOT-jar-with-dependencies.jar stat1005.eqiad.wmnet:~/flink-1.15.0
```

Start a Flink cluster on YARN with
```
export HADOOP_CLASSPATH=`hadoop classpath`
./bin/yarn-session.sh --detached
```

Finally launch the job with
```bash
./bin/flink run -c org.wikimedia.mediawiki.event.enrichment.Enrichment enrichment-1.0-SNAPSHOT-jar-with-dependencies.jar
```

## View the output of a Flink job

On YARN stdout is directed to the container job, and won't be visible from the cli.
We can display container output by accessing its logs with
```
yarn logs -applicationId <applicationId> -containerId <containerId>
```
Where
- `<applicationId>` is the Flink cluster id returned by `yarn-session.sh`, and visible at https://yarn.wikimedia.org.
- `<containerId>` is the container running a specific task, that you can find in Flink's Task Manager at `https://yarn.wikimedia.org/proxy/<applicationId>/#/task-manager`.

For more details see the [project doc](https://ci.apache.org/projects/flink/flink-docs-release-1.13/docs/deployment/resource-providers/standalone/overview/).
The [Flink Web Interface]() will be available at yarn.wikimedia.org under
`https://yarn.wikimedia.org/proxy/<applicationId>`.
# Config

There's a couple of gotchas.

## JVM

We need to rewrite the `Host` HTTP header to properly route HTTP request from the
internal YARN cluster to https://api-ro.discovery.wmnet.

To do so, we need to configure the JVM http-client to allow restricted headers.

Add the following to `conf/flink-conf.yaml`:
```
env.java.opts: -Dsun.net.http.allowRestrictedHeaders=true
```

## Kerberos
Kerberos authentication is required to access WMF Analytics resources.
The relevant config settings are found in `conf/flink-conf.yaml`:
```properties
security.kerberos.login.use-ticket-cache: true
# security.kerberos.login.keytab:
security.kerberos.login.principal: krbtgt/WIKIMEDIA@WIKIMEDIA
# The configuration below defines which JAAS login contexts
security.kerberos.login.contexts: Client,KafkaClient
```