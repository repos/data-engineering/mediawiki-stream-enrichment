package org.wikimedia.mediawiki.event.enrichment

import io.circe.generic.extras.Configuration
import io.circe.generic.extras.auto._
import io.circe.parser._

import scala.util.Try

/* Contains content related data. */
case class Content(
    title: String,
    article_id: Long,
    contentmodel: Option[String],
    contentformat: Option[String],
    content: Option[String]
)

/** Parse the output of Action API response and extract content data.
  */
object Content {
    // Derives a Circe config for interoperability with encode/decode methods.
    // E.g. provides an implicit value for evidence parameter of
    // type io.circe.Decoder[ActionQueryResponse]
    implicit val config: Configuration = Configuration.default.withDefaults

    def from(body: String): Try[Content] = Try {
        decode[ActionQueryResponse](body) match {
            case Left(error) =>
                // TODO(gmodena, 2022-05-31): this should trigger a retry.
                throw new IllegalArgumentException(error)
            case Right(actionQueryResponse: ActionQueryResponse) =>
                extractContent(actionQueryResponse)
        }
    }

    private def extractContent(
        actionQueryResponse: ActionQueryResponse
    ): Content = {
        // TODO(gmodena, 2022-05-31) do we need to validate slots?
        // Maybe we can push these checks to the Json Decoder.
        if (actionQueryResponse.query.pages.isEmpty) {
            throw new NoSuchElementException(
              "Page data is missing from payload")
        }

        // TODO(gmodena, 2022-11-15): in the future we'll need to retrieve and
        // enrich all slots.
        if (actionQueryResponse.query.pages.head.revisions.isEmpty) {
            throw new NoSuchElementException(
              "Revision data is missing from payload")
        }

        val page = actionQueryResponse.query.pages.head
        val revisions = page.revisions.head.slots.main

        new Content(
          title = page.title,
          article_id = page.pageid,
          contentmodel = Option(revisions.contentmodel),
          contentformat = Option(revisions.contentformat),
          content = Option(revisions.content)
        )
    }
}
