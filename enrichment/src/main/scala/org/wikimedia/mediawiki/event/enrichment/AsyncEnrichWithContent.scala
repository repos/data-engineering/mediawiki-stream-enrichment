package org.wikimedia.mediawiki.event.enrichment

import com.google.common.base.Strings
import com.google.common.hash.Hashing
import io.findify.flink.api.async.{AsyncFunction, ResultFuture}
import org.apache.flink.api.common.typeinfo.Types
import org.apache.flink.api.java.tuple
import org.apache.flink.api.java.typeutils.TupleTypeInfo
import org.apache.flink.types.{Row, RowKind}
import org.slf4j.LoggerFactory
import org.wikimedia.eventutilities.flink.EventRowTypeInfo
import org.wikimedia.mediawiki.event.enrichment.wmfapi.{AsyncHttpClient, Query}

import java.math.BigInteger
import java.nio.charset.StandardCharsets
import scala.concurrent.ExecutionContext
import scala.language.implicitConversions
import scala.util.{Failure, Success}

/** An asynchronous Flink call back that performs an async query to the
  * Mediawiki Action API. TODO(gmodena, 2022-06-31): implement retry logic and
  * better error propagation
  */
class AsyncEnrichWithContent(
    val actionApiEndpoint: String,
    val rowTypeInfo: EventRowTypeInfo
) extends AsyncFunction[Row, tuple.Tuple2[String, Row]] {
    private val Log = LoggerFactory.getLogger(classOf[AsyncEnrichWithContent])

    /** * Asynchronoyusly process an imput message.
      *
      * @param event
      *   a Flink page_change Row.
      * @param resultFuture:
      *   a tuple that mimicks Either. Returns an error string (left) or a valid
      *   Row (right). Used in place of Either for ease of serilization. See
      *   https://gitlab.wikimedia.org/repos/data-engineering/mediawiki-stream-enrichment/-/merge_requests/12#note_9718
      */
    override def asyncInvoke(
        event: Row,
        resultFuture: ResultFuture[tuple.Tuple2[String, Row]]
    ): Unit = {
        val change = event.getField("page_change_kind")
        if (
          List(
            ChangeType.PageDelete,
            ChangeType.PageUndelete,
            ChangeType.PageMove,
            ChangeType.PageVisibilityChange).contains(change)
        ) {
            // Events that do not create a new revision payload. There's not content to retrieve.
            collectEvent(enrichWithoutContent(event), resultFuture)
        } else if (
          List(ChangeType.PageCreate, ChangeType.RevisionCreate).contains(
            change)
        ) {
            // Events that create a revision payload. Fetch wikitext from Action API.
            asyncApiCallThenCollectEvent(event, resultFuture)
        } else {
            // Unknown event. Report an error.
            collectEvent(Left(s"Unknown change kind: ${change}"), resultFuture)
        }
    }

    private def enrichWithoutContent(event: Row): Either[String, Row] = {
        try {
            val enrichedEvent = rowTypeInfo.projectFrom(event, true)
            setRowKindType(enrichedEvent)
            Right(enrichedEvent)
        } catch {
            case e: IllegalArgumentException =>
                Left(e.toString)
        }
    }

    /** Send an async request to the Action API and return an enriched event.
      *
      * @param event
      *   a case class that contains a parametrized action api query url and
      *   event payload
      * @param resultFuture
      *   a placeholder for a future enriched event
      */
    private def asyncApiCallThenCollectEvent(
        event: Row,
        resultFuture: ResultFuture[tuple.Tuple2[String, Row]]
    ): Unit = {
        AsyncHttpClient
            .send(makeHttpClientForActionApi(event))
            .map(response => {
                response.body match {
                    case Right(message) =>
                        collectEvent(
                          enrichWithContent(event, message),
                          resultFuture)
                    case Left(error) =>
                        collectEvent(Left(error), resultFuture)
                }
            })(ExecutionContext.global)
    }

    // TODO move this out
    private def makeHttpClientForActionApi(event: Row): Query = {
        val domain: String = event.getFieldAs[Row]("meta").getFieldAs("domain")
        val revision = event.getFieldAs[Row]("revision")
        val revId: Long = revision.getFieldAs[Long]("rev_id")
        val uri = buildRevisionContentQueryUri(revId, url = actionApiEndpoint)
        val headers = Option(Map("Host" -> domain, "User-Agent" -> UserAgent))
        Log.debug(s"Preparing to call $uri from Host: $domain")
        Query(uri, headers)
    }

    /** Enrich an event with content retrieved from an Action API query.
      *
      * @param event
      *   a page Row representing a page change event
      * @param message
      *   revision content, when available
      * @return
      */
    private def enrichWithContent(
        event: Row,
        message: String
    ): Either[String, Row] = {
        Log.debug(message)
        Content.from(message) match {
            case Failure(error) =>
                val errorMsg = error.toString
                // TODO(gmodena, 2022-01-02): trigger a retry
                Log.error(s"Content decoding failed with $errorMsg")
                Left(errorMsg)
            case Success(content) =>
                projectAndSetContentBody(event, content)
        }
    }

    private def setRowKindType(event: Row): Unit = {
        val changeLogKind: String = event.getFieldAs("changelog_kind")

        if (changeLogKind == "insert") {
            event.setKind(RowKind.INSERT)
        } else if (changeLogKind == "delete") {
            event.setKind(RowKind.DELETE)
        } else if (changeLogKind == "update") {
            event.setKind(RowKind.UPDATE_AFTER)
        } else {
            throw new IllegalArgumentException(
              s"unknown changelog_kind ${changeLogKind}")
        }
    }

    private def projectAndSetContentBody(
        event: Row,
        content: Content
    ): Either[String, Row] = {
        val enrichedEvent = rowTypeInfo.projectFrom(event, true)

        try {
            setRowKindType(enrichedEvent)
            val revision: Row =
                enrichedEvent
                    .getFieldAs[Row]("revision")

            // TODO: maybe we should extract utility methods to navigate the page_change schema.
            val contentSlots: java.util.HashMap[String, Row] = {
                revision.getFieldAs("content_slots")
            }
            val main: Row = contentSlots.get("main")
            val checksum: String = main.getFieldAs("content_sha1")
            val contentChecksum =
                checkContentBodySha(content.content.get, checksum)

            if (!contentChecksum) {
                val errorMessage = s"content_body: invalid checksum for " +
                    s"rev_id=${revision.getFieldAs[String]("rev_id")}\t " +
                    s"domain=${enrichedEvent.getFieldAs(
                      event.getFieldAs[Row]("meta").getFieldAs[String]("domain"))}" +
                    s"content_sha1=$checksum" +
                    s"checksum=$contentChecksum"
                Left(errorMessage)
            } else {
                main.setField("content_body", content.content.orNull)
                contentSlots.put("main", main)
                revision.setField("content_slots", contentSlots)
                enrichedEvent.setField("revision", revision)
                Right(enrichedEvent)
            }

        } catch {
            case e: IllegalArgumentException =>
                Left(e.toString)
        }
    }

    private def checkContentBodySha(
        content: String,
        checksum: String
    ): Boolean = {
        val base36Checksum = convertBase(
          Hashing.sha1().hashString(content, StandardCharsets.UTF_8).toString,
          36,
          31
        )
        base36Checksum == checksum
    }

    // scalastyle:off line.size.limit
    // Mediawiki sha1 checksum are base36 encoded and zero-padded to 31 chars length.
    // https://doc.wikimedia.org/mediawiki-core/master/php/SlotRecord_8php_source.html#l00556
    // https://gerrit.wikimedia.org/g/mediawiki/vendor/+/c986711ed8dd0afd0556f361c0189c306999f0ce/wikimedia/base-convert/src/Functions.php#40
    private def convertBase(
        input: String,
        destinationBase: Int,
        minLength: Int
    ): String = {
        val converted = new BigInteger(input, 16).toString(destinationBase)
        Strings.padStart(converted, minLength, '0')
    }

    private def collectEvent(
        response: Either[String, Row],
        resultFuture: ResultFuture[tuple.Tuple2[String, Row]]
    ): Unit = {
        val transformed = new tuple.Tuple2[String, Row];
        response match {
            case Right(message) =>
                transformed.f1 = message;
            case Left(error) =>
                transformed.f0 = error
                transformed.f1 = rowTypeInfo.createEmptyRow()
        }
        resultFuture.complete(Iterable(transformed))
    }

    def outputTypeInfo: TupleTypeInfo[tuple.Tuple2[String, Row]] = {
        new TupleTypeInfo[tuple.Tuple2[String, Row]](Types.STRING, rowTypeInfo)
    }
}
