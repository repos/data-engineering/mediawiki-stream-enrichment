package org.wikimedia.mediawiki.event

import io.findify.flink.api.OutputTag
import org.apache.flink.api.common.typeinfo.Types
import org.wikimedia.eventutilities.flink.stream.EventDataStreamFactory
import org.wikimedia.mediawiki.event.enrichment.wmfapi.QueryBuilder

import java.util.Properties
import scala.collection.immutable.ListMap
import scala.jdk.CollectionConverters._

/** This package object contains a few utilities that don't have a well defined
  * place yet.
  */
//noinspection ScalaDeprecation
package object enrichment {

    // Maximum allowed length for a page change content payload.
    // https://gerrit.wikimedia.org/g/operations/puppet/+/158777d843d3d6709fb33da35571794dadc52acf/modules/profile/manifests/kafka/mirror.pp#203
    val EnrichedEventOverheadMaxBytes =
        128 // budget some space for the new field names

    val KafkaMessageMaxBytes = 4194304
    val KafkaMessageWithContentMaxBytes =
        KafkaMessageMaxBytes - EnrichedEventOverheadMaxBytes

    val KafkaCompressionType = "snappy"
    val KafkaLingerMs = 1000 // delays sends, so messages can be batched

    // URL of the internal Action API endpoint.
    // See: https://phabricator.wikimedia.org/T300977#7700803
    val MetaWikimedia = "https://meta.wikimedia.org"
    val ApiRoDiscovery = "https://api-ro.discovery.wmnet"
    val WmfInternalActionApiEndpoint = s"$ApiRoDiscovery/w/api.php"

    implicit var actionApiEndpoint = WmfInternalActionApiEndpoint

    // Broadcast to Mediawiki that Mediawiki Stream Enrichment requests are non-human traffic.
    val UserAgent = "wmf-mediawiki-stream-enrichment/1.0-SNAPSHOT bot"
    val PageChangeTopicPrefix = s"eqiad.mediawiki"
    val RevisionCreateWithContentPrefix = "mediawiki"
    val ErrorSideOutputTag = new OutputTag[String]("errors")(Types.STRING)

    // KafkaSource additional properties. It holds config for both Flink Source (Tables) as well
    // as Kafka brokers.
    val eventStreamSourceProperties: Properties = new Properties()
    eventStreamSourceProperties.putAll(
      Map(
        "scan.startup.mode" -> "latest-offset", // tell flink to use offsets if they exist,
        "properties.auto.offset.reset" -> "latest" // tell kafka where to start if offsets don’t exist
      ).asJava
    )

    // KafkaSource additional properties. It holds config for both Flink Source (Tables) as well
    // as Kafka brokers.
    val eventStreamProducerProperties: Properties = new Properties()
    eventStreamProducerProperties.putAll(
      Map("max.request.size" -> s"$KafkaMessageMaxBytes").asJava
    )

    val PageChangeTopic = "eqiad.rc0.mediawiki.page_change"
    val PageChangeStreamVersion = "1.1.0"
    val PageChangeStreamName = "rc0.mediawiki.page_change"
    val PageChangeContentTopic = "eqiad.rc0.mediawiki.page_content_change"
    val PageChangeContentStreamVersion = "1.1.0"
    val PageChangeContentStreamName = "rc0.mediawiki.page_content_change"
    val httpClientRoutes = Map(MetaWikimedia -> ApiRoDiscovery)

    // Config copied from https://gitlab.wikimedia.org/repos/data-engineering/mediawiki-stream-enrichment/-/merge_requests/6
    // TODO(otto, 2022-05-27) parameterize these in via config or main args.
    // These are the WMF production network values and will not work anywhere else.
    private val streamConfigUri = "https://meta.wikimedia.org/w/api.php"
    private val schemaBaseUris = Seq(
      "https://schema.discovery.wmnet/repositories/primary/jsonschema",
      "https://schema.discovery.wmnet/repositories/secondary/jsonschema"
    )

    /** An helper method query the Action API and retrive revision content
      *
      * @param revisionId
      *   the target page revision
      */
    def buildRevisionContentQueryUri(
        revisionId: Long,
        url: String = "https://api-ro.discovery.wmnet/w/api.php"
    ): String = {
        val params: Map[String, String] = ListMap(
          "action" -> "query",
          "format" -> "json",
          "formatversion" -> "2",
          "prop" -> "revisions",
          "revids" -> revisionId.toString,
          "rvprop" -> "content",
          "rvslots" -> "main"
        )

        QueryBuilder(url).addParams(params).toString()
    }

    object KafkaConfig {
        val DataCenter = "eqiad"
        val BootstrapServers = s"kafka-jumbo1001.${DataCenter}.wmnet:9092"
        val ConsumerGroupId = "mediawiki-stream-enrichment"
    }

    implicit lazy val eventStreamFactory: EventDataStreamFactory =
        EventDataStreamFactory.from(
          schemaBaseUris.asJava,
          streamConfigUri,
          httpClientRoutes.asJava
        )
}
