package org.wikimedia.mediawiki.event.enrichment.wmfapi

import sttp.client3._
import sttp.model.Uri

/** TODO(gmodena, 2022-05-18) we should put a generic component to
  * https://gerrit.wikimedia.org/r/plugins/gitiles/wmf-jvm-utils/+/refs/heads/master/http-client-utils/
  * that detects a wmf domain and automatically do the routing to
  * api-ro.discovery.wmnet so that api users don't have to bother about about
  * crafting the host header
  */
/** Helper class to build query strings for the Action API. */
class QueryBuilder(private val url: String) {
    private val endpoint = uri"$url"

    /** Append query parameters to the `endpoint` url.
      *
      * @param paramsMap
      *   a map of query parameters
      * @return
      *   the endpoint URL with query string.
      */
    def addParams(paramsMap: Map[String, String]): Uri =
        endpoint.addParams(paramsMap)
}

object QueryBuilder {
    def apply(url: String): QueryBuilder = new QueryBuilder(url)
}

/** `ActionQuery` specifies input parameters for a callback to Mediawiki's
  * Action API. Namely, the query string and (optionally) the domain name of the
  * server
  *
  * @param uri
  *   the target URL containing and endpoint and query string parameters.
  * @param headers
  *   additional HTTP headers (eg. Host: domain). When using the internal
  *   https://api-ro.discovery.wmnet/w/api.php endpoint, `headers` should
  *   contain `Host`.
  *
  * TODO(gmodena, 2022-05-20) it maybe makes more sense to have uri be a
  * sttp.model.Uri, and avoid multiple conversions. Unfortunately, the type is
  * not serializable in DataStream. Sticking to String for now.
  */
case class Query(uri: String, headers: Option[Map[String, String]])
