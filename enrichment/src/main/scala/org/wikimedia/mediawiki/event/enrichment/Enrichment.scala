package org.wikimedia.mediawiki.event.enrichment

import io.findify.flink.api._
import org.apache.flink.api.common.eventtime.WatermarkStrategy
import org.apache.flink.api.connector.sink2.Sink
import org.apache.flink.api.java.tuple.Tuple2
import org.apache.flink.connector.kafka.sink.KafkaSink
import org.apache.flink.connector.kafka.source.KafkaSource
import org.apache.flink.streaming.api.functions.ProcessFunction
import org.apache.flink.types.Row
import org.apache.flink.util.Collector
import org.slf4j.LoggerFactory
import org.wikimedia.eventutilities.flink.formats.json.KafkaRecordTimestampStrategy
import org.wikimedia.eventutilities.flink.stream.EventDataStreamFactory
import org.wikimedia.mediawiki.event.enrichment.wmfapi.Query

import java.util.concurrent.TimeUnit
import scala.language.implicitConversions

case class EventEnrichment[+T](endpointUrl: Query, event: T)

class Enrichment

/** A streaming job that listens for page changes, queries the Action API for
  * page content changes, and prints them to stdout.
  *
  * Note that:
  *   - Currently it uses the DataStream API to process page changes. . Event
  *     schema boilerplate for supported event types is defined in
  *     `org.wikimedia.dataplatform.events.PageChange`.
  *   - Calls to the Action API are performed in a `AsyncDataStream`.
  *   - Some Kafka boilerplate classes and a query builder helper are defined in
  *     the `dataplatform` package object.
  */
object Enrichment {
    lazy val env: StreamExecutionEnvironment =
        StreamExecutionEnvironment.getExecutionEnvironment
    private val Log = LoggerFactory.getLogger(classOf[Enrichment])
    // Flink application identifier.
    private val JobName = "Mediawiki Page Content Stream Enrichment"
    // Timeout (milliseconds) for an AsyncFunction to complete.
    private val AsyncFunctionTimeoutMs = 120000
    // The max number of async i/o operations in progress at the same time in an AsyncDataStream instance.
    // This is set to a safe value. It assumes the application is running on a default Flink config with
    // - parallelism.default=1
    // - taskmanager.numberOfTaskSlots=1
    // Both can be adjusted in conf/flink.conf.
    // See https://nightlies.apache.org/flink/flink-docs-release-1.15/docs/deployment/config/ for more details.
    private val AsyncDataStreamCapacity = 12

    def main(args: Array[String]): Unit = {
        val source = getKafkaEventStreamSource(PageChangeStreamName)
        val sink: KafkaSink[Row] =
            getKafkaEventStreamSink(
              PageChangeContentStreamName,
              PageChangeContentStreamVersion
            )

        makePipeline(source = source, sink = sink)(
          actionApiEndpoint,
          eventStreamFactory
        )

        env.execute(JobName)
    }

    /** Returns a DataStream[Row] of the event stream in Kafka Cluster
      * configured KafkaConfig.
      */
    private def getKafkaEventStreamSource(
        streamName: String
    )(implicit eventStreamFactory: EventDataStreamFactory): DataStream[Row] = {
        val eventSource: KafkaSource[Row] =
            eventStreamFactory
                .kafkaSourceBuilder(
                  streamName,
                  KafkaConfig.BootstrapServers,
                  KafkaConfig.ConsumerGroupId
                )
                .setProperties(eventStreamSourceProperties)
                .build();
        env.fromSource(
          eventSource,
          WatermarkStrategy.noWatermarks(),
          PageChangeTopic
        )(eventSource.getProducedType)
    }

    // scalastyle:off null
    private def getKafkaEventStreamSink(
        streamName: String,
        schemaVersion: String
    )(implicit eventStreamFactory: EventDataStreamFactory): KafkaSink[Row] =
        eventStreamFactory
            .kafkaSinkBuilder(
              streamName,
              schemaVersion,
              KafkaConfig.BootstrapServers,
              PageChangeContentTopic,
              KafkaRecordTimestampStrategy.ROW_EVENT_TIME
            )
            .setKafkaProducerConfig(eventStreamProducerProperties)
            .build()

    /** A helper method to assemble an enrichment pipeline
      *
      * actionApiEndpoint and eventStreamFactory are injected so that we can hit
      * arbitrary endpoints, and inject ad-hoc schemas for Row SerDe.
      *
      * @param source
      *   a datastream of page change events.
      * @param actionApiEndpoint
      *   the action API endpoint to use.
      * @param eventStreamFactory
      *   the event stream factory for `source.
      * @return
      */
    def makePipeline(source: DataStream[Row], sink: Sink[Row])(implicit
        actionApiEndpoint: String,
        eventStreamFactory: EventDataStreamFactory
    ): Unit = {

        /** Perform enrichment in an async stream that is the union of changes.
          *
          * For events that carry revision content (page-create,
          * revision-create) a query to the Action API will retrieve it. For
          * events that do not carry revision content, simplfy push the event
          * forward and enrich without any api call.
          */
        val enrichedStream = enrichPageChange(eventStream = source)
        enrichedStream.sinkTo(sink)
        enrichedStream
            .getSideOutput(ErrorSideOutputTag)(ErrorSideOutputTag.getTypeInfo)
            .addSink(LoggerFactory.getLogger("enrichment-errors").error(_))
    }

    /** Enrich an event stream with content retrieved from Wikimedia Action API.
      *
      * @param eventStream
      *   a stream of page change events
      * @param actionApiEndpoint
      *   the action API endpoint to use.
      * @param eventStreamFactory
      *   the event stream factory for eventStream.
      * @return
      */
    def enrichPageChange(eventStream: DataStream[Row])(implicit
        actionApiEndpoint: String,
        eventStreamFactory: EventDataStreamFactory
    ): DataStream[Row] = {
        val typeInfo =
            eventStreamFactory.rowTypeInfo(
              PageChangeContentStreamName,
              PageChangeContentStreamVersion
            )

        val asyncOp = new AsyncEnrichWithContent(
          actionApiEndpoint = actionApiEndpoint,
          rowTypeInfo = typeInfo
        )

        val stream = AsyncDataStream.unorderedWait(
          filterEventsBySize(withoutCanaryEvents(eventStream)),
          asyncOp,
          timeout = AsyncFunctionTimeoutMs,
          TimeUnit.MILLISECONDS,
          AsyncDataStreamCapacity
        )(asyncOp.outputTypeInfo)

        // Route errors to a side output
        stream.process(
          (
              event: Tuple2[String, Row],
              context: ProcessFunction[Tuple2[String, Row], Row]#Context,
              collector: Collector[Row]
          ) => {
              (Option(event.f0), Option(event.f1)) match {
                  case (Some(error), _) =>
                      context.output(ErrorSideOutputTag, error)
                  case (None, Some(row)) => collector.collect(row)
              }
          })(typeInfo)
    }

    private def filterEventsBySize(source: DataStream[Row]): DataStream[Row] = {
        source
            .filter(((pageChange: Row) => {
                val revSlots: Row =
                    pageChange.getFieldAs("revision")
                val revLen: Long = revSlots.getFieldAs("rev_size")
                // TODO we should also account for the extra fields added
                val messageSize =
                    pageChange.toString.getBytes("UTF-8").size + revLen
                if (messageSize >= KafkaMessageWithContentMaxBytes) {
                    // See
                    // https://gerrit.wikimedia.org/g/operations/puppet/+/158777d843d3d6709fb33da35571794dadc52acf/modules/profile/manifests/kafka/mirror.pp#203
                    Log.info(
                      s"Discarded event for ${revSlots.getField("rev_id")}" +
                          s"The enriched event will exceed kafka's producer_request_max_size. Event size: $messageSize"
                    )
                }
                messageSize < KafkaMessageWithContentMaxBytes
            })) // TODO(gmodena, 2022-06-05): log and emit to SideOutput?
    }

    private def withoutCanaryEvents(
        source: DataStream[Row]
    ): DataStream[Row] = {
        source
            .filter(((event: Row) => {
                val domain =
                    event.getFieldAs[Row]("meta").getFieldAs[String]("domain")
                domain != "canary"
            }))
    }
}
