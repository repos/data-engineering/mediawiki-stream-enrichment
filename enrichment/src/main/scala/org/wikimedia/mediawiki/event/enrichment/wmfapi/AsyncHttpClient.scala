package org.wikimedia.mediawiki.event.enrichment.wmfapi

import org.slf4j.LoggerFactory
import sttp.client3._
import sttp.client3.asynchttpclient.future.AsyncHttpClientFutureBackend

import scala.concurrent.Future
import scala.concurrent.duration.DurationInt

class AsyncHttpClient

object AsyncHttpClient {
    private final val ConnectionTimeout = 2.second
    private final val ReadTimeout = 60.second

    /** There exists only one instance of the AsyncFunction and it is called
      * sequentially for each record in the respective partition of the stream.
      * Unless the asyncInvoke(...) method returns fast and relies on a callback
      * (by the client), it will not result in proper asynchronous I/O.
      *
      * Therefore we query Mediawiki with an async http client.
      */
    private lazy val backend = AsyncHttpClientFutureBackend(
      options = SttpBackendOptions.connectionTimeout(ConnectionTimeout)
    )
    private val Log = LoggerFactory.getLogger(classOf[AsyncHttpClient])

    def apply(): AsyncHttpClient = new AsyncHttpClient()

    /** Query asynchronously.
      *
      * @param query
      *   a case class containing the endpoint URL and optional http headers
      * @return
      */
    def send(query: Query): Future[Response[Either[String, String]]] = {
        val headersMap = query.headers.getOrElse(Map())
        val request = basicRequest.headers(headersMap).get(uri"${query.uri}")
        Log.debug(
          s"uri ${request.uri.toString()}\nheaders ${request.headers.toString}"
        )
        request.readTimeout(ReadTimeout).send(backend)
    }
}
