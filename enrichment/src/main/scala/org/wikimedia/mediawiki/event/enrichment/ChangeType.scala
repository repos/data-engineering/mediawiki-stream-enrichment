package org.wikimedia.mediawiki.event.enrichment

object ChangeType {
    final val PageCreate: String = "create"
    final val PageDelete: String = "delete"
    final val RevisionCreate: String = "edit"
    final val PageMove = "move"
    final val PageVisibilityChange = "visibility_change"
    final val PageUndelete = "undelete"
}
