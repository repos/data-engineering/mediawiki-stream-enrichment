package org.wikimedia.mediawiki.event.enrichment

protected case class Pages(
    pageid: Int,
    ns: Int,
    title: String,
    revisions: Seq[Revisions]
)

protected case class Query(pages: Seq[Pages])

protected case class Revisions(slots: Slots)

protected case class Slots(main: Main)

protected case class Main(
    contentmodel: String,
    contentformat: String,
    content: String
)

/** Encapsulate the payload of a successful action=query request, parametrized
  * by the buildRevisionContentQueryUri() helper.
  *
  * @param batchcomplete
  * @param query
  */
case class ActionQueryResponse(batchcomplete: Boolean, query: Query)
