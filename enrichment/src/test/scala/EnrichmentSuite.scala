import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock._
import com.github.tomakehurst.wiremock.core.WireMockConfiguration.options
import com.google.common.io.Resources
import io.findify.flink.api._
import io.findify.flinkadt.api._
import org.apache.flink.runtime.testutils.MiniClusterResourceConfiguration
import org.apache.flink.streaming.api.functions.sink.SinkFunction
import org.apache.flink.test.util.MiniClusterWithClientResource
import org.apache.flink.types.{Row, RowKind}
import org.scalatest.BeforeAndAfter
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.wikimedia.eventutilities.flink.stream.EventDataStreamFactory
import org.wikimedia.mediawiki.event.enrichment.Enrichment.enrichPageChange
import org.wikimedia.mediawiki.event.enrichment.{
    ChangeType,
    ErrorSideOutputTag,
    PageChangeStreamName,
    PageChangeStreamVersion
}

import java.nio.charset.StandardCharsets
import scala.collection.mutable.ListBuffer
import scala.io.Source
import scala.jdk.CollectionConverters._
import scala.language.implicitConversions

// scalastyle:off line.size.limit
class EnrichmentIntegrationTest
    extends AnyFlatSpec
    with Matchers
    with BeforeAndAfter {
    val flinkCluster = new MiniClusterWithClientResource(
      new MiniClusterResourceConfiguration.Builder()
          .setNumberSlotsPerTaskManager(1)
          .setNumberTaskManagers(1)
          .build
    )
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    val JobName = "MediawikiStreamEnrichmentMiniCluster"
    val localEndpointUrl = "http://localhost:8888/w/api.php"
    val wm: WireMockServer = new WireMockServer(options.port(8888))
    val pageCreateRevId = 3270050
    val revisionCreateRevId = 3270051
    val pageDeleteRevId = 548599
    val revisionCreateMissingRevId = 2147483647 //Int.MaxValue
    val pageCreateTestUrl: String = mkTestUrl(pageCreateRevId)
    val revisionCreateTestUrl: String = mkTestUrl(revisionCreateRevId)
    val missingRevisionTestUrl: String = mkTestUrl(revisionCreateMissingRevId)
    val pageChangeResourcePath: String =
        "src/test/resources/data/mediawiki/page/change/"
    val pageChangeEvents =
        List(
          "create.json", // insert changelog kind
          "edit.json", // update changelog kind
          "delete.json", // delete changelog kind
          "canary.json", // insert changelog kind (canary event)
          "unknown.json" // unknown changelog kind (canary event to track regressions)
        )
    val pageCreateBody: String =
        """|{
           |  "batchcomplete": true,
           |  "query": {
           |    "pages": [
           |      {
           |        "pageid": 322194,
           |        "ns": 0,
           |        "title": "EnrichmentTestPage",
           |        "revisions": [
           |          {
           |            "sha1": "7f925b489ba9654866795685b450feb79ce2158f",
           |            "slots": {
           |              "main": {
           |                "contentmodel": "wikitext",
           |                "contentformat": "text/x-wiki",
           |                "content": "Add test page for mw enrichment pipeline."
           |              }
           |            }
           |          }
           |        ]
           |      }
           |    ]
           |  }
           |}
           |""".stripMargin
    val revisionCreateBody: String =
        """
          |{
          |  "batchcomplete": true,
          |  "query": {
          |    "pages": [
          |      {
          |        "pageid": 322194,
          |        "ns": 0,
          |        "title": "EnrichmentTestPage",
          |        "revisions": [
          |          {
          |            "sha1": "e8c03dcdfd1930f928cddd7697ff6cf61fc93c7f",
          |            "slots": {
          |              "main": {
          |                "contentmodel": "wikitext",
          |                "contentformat": "text/x-wiki",
          |                "content": "Test page for mw enrichment pipeline."
          |              }
          |            }
          |          }
          |        ]
          |      }
          |    ]
          |  }
          |}
          |""".stripMargin
    val missingRevisionIdBody: String =
        """
          |{
          |    "batchcomplete": true,
          |    "query": {
          |        "badrevids": {
          |            "2147483647": {
          |                "revid": 2147483647,
          |                "missing": true
          |            }
          |        }
          |    }
          |}
          |""".stripMargin

    private def mkTestUrl(revisionId: Int): String =
        s"/w/api.php?action=query&format=json&formatversion=2&prop=revisions&revids=${revisionId}&rvprop=content&rvslots=main"

    before {
        ErrorSink.errors.clear()
        flinkCluster.before()
        env.setParallelism(1)

        wm.stubFor(
          get(pageCreateTestUrl)
              .willReturn(
                ok.withHeader("Content-Type", "application/json")
                    .withBody(pageCreateBody)
              )
        )

        wm.stubFor(
          get(revisionCreateTestUrl)
              .willReturn(
                ok.withHeader("Content-Type", "application/json")
                    .withBody(revisionCreateBody)
              )
        )

        wm.stubFor(
          get(missingRevisionTestUrl)
              .willReturn(
                ok.withHeader("Content-Type", "application/json")
                    .withBody(missingRevisionIdBody)
              )
        )

        wm.start()
    }

    after {
        flinkCluster.after()
        wm.stop()
    }

    "Page changes" should "should be enriched with calls to the api endpoint" in {
        val testStreamConfigsFile =
            Resources.getResource("event_stream_configs.json").toString
        val schemaBaseUris =
            Seq(Resources.getResource("schemas/").toString)

        val eventStreamFactory =
            EventDataStreamFactory.from(
              schemaBaseUris.asJava,
              testStreamConfigsFile)

        val pageChanges: List[String] = {
            pageChangeEvents.map((event: String) => {
                val bufferedSource =
                    Source.fromFile((s"$pageChangeResourcePath/${event}"))
                val data = bufferedSource.mkString
                bufferedSource.close()
                data
            })
        }

        // 1 page create
        // 1 valid edit
        // 1 invalid edit
        // 1 page delete
        // 1 canary event
        // 1 unknown changelog type
        val inputEvents = 6
        assert(
          pageChanges
              .flatMap((s: String) => s.split("\n"))
              .length == inputEvents
        )

        val deserializer =
            eventStreamFactory.deserializer(
              PageChangeStreamName,
              PageChangeStreamVersion
            )
        val dataStream: DataStream[Row] = env
            .fromCollection(pageChanges)
            .flatMap((events: String) => events.split("\n"))
            .map((jsonString: String) =>
                deserializer.deserialize(
                  jsonString.getBytes(StandardCharsets.UTF_8)))(
              deserializer.getProducedType)

        val contentStream =
            enrichPageChange(dataStream)(localEndpointUrl, eventStreamFactory)
        contentStream.getSideOutput(ErrorSideOutputTag).addSink(ErrorSink)

        val enriched = contentStream.executeAndCollect().to(LazyList)

        // we expect 3 successes and one error, caused by failing to enrich
        // an edit with missing revision id.
        // A canary event (page create in canary.json) should be filtered out.
        val validEvents = 3
        val invalidEvents = 1
        assert(enriched.length == validEvents)
        assert(ErrorSink.errors.size == invalidEvents)

        enriched.foreach(event => {
            val content: Option[String] = Option(
              event
                  .getFieldAs[Row]("revision")
                  .getFieldAs[java.util.HashMap[String, Row]]("content_slots")
                  .get("main")
                  .getFieldAs[String]("content_body")
            )

            val revId = event
                .getFieldAs[Row]("revision")
                .getField("rev_id")
            val changeType = event.getField("page_change_kind")
            val domain =
                event.getFieldAs[Row]("meta").getFieldAs[String]("domain")

            assert(revId != revisionCreateMissingRevId)
            assert(domain != "canary")
            if (revId === pageCreateRevId) {
                assert(changeType === ChangeType.PageCreate)
                assert(content.isDefined)
                assert(event.getKind == RowKind.INSERT)
            } else if (revId === revisionCreateRevId) {
                assert(changeType === ChangeType.RevisionCreate)
                assert(content.isDefined)
                assert(event.getKind == RowKind.UPDATE_AFTER)
            } else if (revId === pageDeleteRevId) {
                assert(changeType === ChangeType.PageDelete)
                assert(event.getKind == RowKind.DELETE)
            }
        })
    }

    "Calls to the mocked endpoint" should "be triggered exactly once" in {
        val expectedGetRequestCount = 1
        wm.verify(
          expectedGetRequestCount,
          getRequestedFor(urlEqualTo(pageCreateTestUrl))
        ) // Triggered by one call to executeAndCollect() from the global env
        wm.verify(
          expectedGetRequestCount,
          getRequestedFor(urlEqualTo(revisionCreateTestUrl))
        ) // Triggered by one call to executeAndCollect() from the global env
        wm.verify(
          expectedGetRequestCount,
          getRequestedFor(urlEqualTo(missingRevisionTestUrl))
        ) // Triggered by one call to executeAndCollect() from the global env
    }
}

/** Static object to collect errors. It must be static as those are collected
  * from another thread and would be serialized to the task executor thread.
  */
object ErrorSink extends SinkFunction[String] {
    val errors: ListBuffer[String] = new ListBuffer

    override def invoke(value: String, context: SinkFunction.Context): Unit =
        errors.append(value)
}
